resource "aws_vpc" "DEMO" {
  cidr_block = "${var.vpc_cidr}"
  tags {
    Name = "DEMO-${terraform.workspace}"
  }
}
resource "aws_internet_gateway" "internet_gateway" {
  depends_on = ["aws_vpc.DEMO"]
  vpc_id = "${aws_vpc.DEMO.id}"
}
resource "aws_route_table" "route-table" {
  vpc_id = "${aws_vpc.DEMO.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.internet_gateway.id}"
  }
}
resource "aws_route_table_association" "SubnetRTAssociation" {
  route_table_id = "${aws_route_table.route-table.id}"
  subnet_id      = "${aws_subnet.public_subnets.0.id}"
}
