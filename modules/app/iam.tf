resource "aws_iam_role" "IAMRole" {
  count = "${terraform.workspace == "dev" ? 1 : 0}"
  name = "s3role-${terraform.workspace}"
  path = "/"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
  tags {
    Name = "s3role-${terraform.workspace}"
  }
}
resource "aws_iam_policy" "s3policy" {
  count = "${terraform.workspace == "dev" ? 1 : 0}"
  name   = "s3policy"
  path   = "/"
  policy = "${data.aws_iam_policy_document.s3policy.json}"
}
resource "aws_iam_role_policy_attachment" "role-policy-attachment" {
  count = "${terraform.workspace == "dev" ? 1 : 0}"
  policy_arn = "${aws_iam_policy.s3policy.arn}"
  role       = "${aws_iam_role.IAMRole.id}"
}
