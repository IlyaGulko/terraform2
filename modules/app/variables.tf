variable "keys" { type = "map" }
variable "vpc_id" {}
variable "vpc_cidr" {}
variable "public_cidrs" { type = "list" }
variable "private_cidrs" { type = "list" }
variable "azs" { type = "list" }
variable "public_subnets_ids" { type = "list" }
variable "aws_amis_n_types" {
  type = "map"
  default = {
    us-east-1 = {
      ami           = "ami-0ac019f4fcb7cb7e6",
      instance_type = "t2.micro"
    }
//    us-west-1 = {
//      ami           = "ami-063aa838bd7631e0b",
//      instance_type = "t2.micro"
//    }
  }
}
data "aws_iam_policy_document" "s3policy" {
  count = "${terraform.workspace == "dev" ? 1 : 0}"
  statement {
    actions   = ["s3:ListBucket", "s3:GetObject", "s3:PutObject"]
    resources = ["${aws_iam_role.IAMRole.arn}"]
  }
}