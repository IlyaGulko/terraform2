resource "aws_launch_configuration" "ASGLaunchConfiguration" {
  count = "${terraform.workspace == "dev" ? 1 : 0}"
  image_id             = "${lookup(var.aws_amis_n_types["us-east-1"], "ami")}"
  instance_type        = "${lookup(var.aws_amis_n_types["us-east-1"], "instance_type")}"
  security_groups      = ["${aws_security_group.SG-EC2.id}"]
  name                 = "demo-app-01-${terraform.workspace}"
  user_data            = <<-USERDATA
                            #!/bin/bash -xe
                            apt-get update
                            apt-get -y install awscli nginx
                            aws configure set aws_access_key_id ${var.keys["aws_access_key"]}
                            aws configure set aws_secret_access_key ${var.keys["aws_secret_key"]}
                            aws s3api get-object --bucket ${aws_s3_bucket.nginx-configuration.bucket} --key index.html /var/www/html/index.html
                            aws s3api get-object --bucket ${aws_s3_bucket.nginx-configuration.bucket} --key nginx.conf /etc/nginx/nginx.conf
                            service nginx restart
                            USERDATA
}
resource "aws_autoscaling_group" "ASG" {
  count = "${terraform.workspace == "dev" ? 1 : 0}"
  depends_on           = ["aws_launch_configuration.ASGLaunchConfiguration"]
  name                 = "demo-app-01-asg-${terraform.workspace}"
  vpc_zone_identifier  = ["${var.public_subnets_ids[0]}"]
  max_size             = 1
  min_size             = 1
  launch_configuration = "${aws_launch_configuration.ASGLaunchConfiguration.id}"
//  initial_lifecycle_hook {
//    lifecycle_transition = "autoscaling:EC2_INSTANCE_LAUNCHING"
//    name                 = "ASGLaunchConfiguration"
//    role_arn             = "${aws_iam_role.IAMRole.arn}"
//    notification_target_arn = "${aws_iam_role.IAMRole.}"
//  }
}
resource "aws_autoscaling_attachment" "asg_attachment" {
  count = "${terraform.workspace == "dev" ? 1 : 0}"
  depends_on             = ["aws_autoscaling_group.ASG", "aws_elb.demo-app-01-elb"]
  autoscaling_group_name = "${aws_autoscaling_group.ASG.id}"
  elb                    = "${aws_elb.demo-app-01-elb.id}"
}