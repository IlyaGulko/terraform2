resource "aws_security_group" "SG-ELB" {
  count = "${terraform.workspace == "dev" ? 1 : 0}"
  name = "SG-ELB-${terraform.workspace}"
  vpc_id = "${var.vpc_id}"
  ingress {
    description = "Accept all HTTP"
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Accept all from private instances"
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["${var.private_cidrs}"]
  }
  ingress {
    description = "Accept all from public instances"
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["${var.public_cidrs}"]
  }
  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_security_group" "SG-EC2" {
  count = "${terraform.workspace == "dev" ? 1 : 0}"
  name    = "SG-EC2-${terraform.workspace}"
  vpc_id  = "${var.vpc_id}"
  ingress {
    description = "Accept all from ELB"
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["${var.vpc_cidr}"]
  }
  ingress {
    description = "Accept all SSH"
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["${var.vpc_cidr}"]
  }
  egress {
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    description = "Send HTTPS"
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    description = "Send HTTP"
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
}