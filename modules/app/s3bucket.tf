resource "aws_s3_bucket" "nginx-configuration" {
  count = "${terraform.workspace == "dev" ? 1 : 0}"
  bucket = "nginx-configuration-${terraform.workspace}"
  region = "us-east-1"
  tags {
    Name = "nginx-configuration"
  }
  provisioner "local-exec" {
    command = "aws s3api put-object --bucket ${aws_s3_bucket.nginx-configuration.bucket} --key nginx.conf --body modules/app/nginx/nginx.conf"
  }
}
resource "aws_s3_bucket_policy" "bucket-policy" {
  count = "${terraform.workspace == "dev" ? 1 : 0}"
  depends_on = ["aws_s3_bucket.nginx-configuration"]
  bucket     = "${aws_s3_bucket.nginx-configuration.id}"
  policy     = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "bucket-policy",
  "Statement": [
    {
      "Sid": "IPAllow",
      "Effect": "Allow",
      "Principal": {"AWS": "${aws_iam_role.IAMRole.arn}"},
      "Action": [
                "s3:PutObject",
                "s3:GetObject"
              ],
      "Resource": "${aws_s3_bucket.nginx-configuration.arn}/*"
    }
  ]
}
POLICY
}
//resource "aws_s3_bucket_object" "nginx-conf" {
//  depends_on = ["aws_s3_bucket.nginx-configuration"]
//  bucket = "${aws_s3_bucket.nginx-configuration.bucket}"
//  key = "nginx.conf"
//  source = "app/nginx/nginx.conf"
//}
resource "aws_s3_bucket_object" "index-html" {
  count = "${terraform.workspace == "dev" ? 1 : 0}"
  depends_on = ["aws_s3_bucket.nginx-configuration"]
  bucket     = "${aws_s3_bucket.nginx-configuration.bucket}"
  key        = "index.html"
  source     = "modules/app/nginx/index.html"
}