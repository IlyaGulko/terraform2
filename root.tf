provider "aws" {
  access_key = "${var.keys["aws_access_key"]}"
  secret_key = "${var.keys["aws_secret_key"]}"
  region     = "${var.aws_region}"
}

terraform {
  backend "s3" {
    bucket = "terraform-config-001"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}

module "vpc" {
  source      = "modules/vpc"
  azs_n_cidrs = "${var.azs_n_cidrs}"
  vpc_cidr    = "${var.vpc_cidr}"
}

module "app" {
  source              = "modules/app"
  keys                = "${var.keys}"
  vpc_id              = "${module.vpc.vpc_id}"
  public_cidrs        = "${module.vpc.public_cidrs}"
  private_cidrs       = "${module.vpc.private_cidrs}"
  azs                 = "${module.vpc.azs}"
  public_subnets_ids  = "${module.vpc.public_subnets_ids}"
  vpc_cidr            = "${module.vpc.vpc_cidr}"
}
